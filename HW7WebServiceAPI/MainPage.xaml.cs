﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace HW7WebServiceAPI
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_GetDictionary(object sender, System.EventArgs e)
        {
            //if there is not internet connection, show a pop-up alert
            if (!CrossConnectivity.Current.IsConnected)
            {
                //You are offline, notify the user
                await DisplayAlert("No Internet", "No internet connection detected", "Ok");
            }
            //if there is internet connection, continue 
            else
            {
                var client = new HttpClient();

                var dictionaryApiAddress = "https://owlbot.info/api/v2/dictionary/" + searchEntry.Text;

                var url = new Uri(dictionaryApiAddress);

                List<DictionaryModel> dictionaryData = new List<DictionaryModel>();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();

                    dictionaryData = JsonConvert.DeserializeObject<List<DictionaryModel>>(jsonContent);
                    //if there are not defitinions, do this
                    if (dictionaryData.Count == 0)
                    {
                        typeLabel.Text = "Missing or not a word";
                        ListView.ItemsSource = null;
                    }
                    //if there are definitions, do this
                    else
                    {
                        typeLabel.Text = "";
                        ListView.ItemsSource = new ObservableCollection<DictionaryModel>(dictionaryData);
                    }
                }
            }
        
        }
    }
}
