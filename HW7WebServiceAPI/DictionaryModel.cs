﻿using System;
using System.Collections.Generic;

namespace HW7WebServiceAPI
{
    public class DictionaryModel
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
    }
}
